

To launch the env, make sure you have virtualbox installed.
https://www.virtualbox.org/wiki/Downloads

Once installed.

- Go into the "Global Tools Menu"
	- Ensure that you have vboxnet0 (Add if needed)
	- Configure vboxnet0 with 192.168.56.1/24
	- Select Apply

Download .ova file using the following link.
https://ulti.box.com/s/zuygzq9g7qe1auf1zf245klha24pnnsz

- Click File, Import Appliance
	- Navigate to the location of the .ova and select Open
	- Edit any fields you need changed.


Lab Info

- Host: Ubuntu 16.04
- Username: ansible
- Password: password01
- Network:
	- SSH Access: 192.168.56.3
	- Nat: 10.0.3.15
- pip 18.0
- ansible 2.7.0
- lxc:
    - server01
    - server02
    - server03
 - base image: base_ubuntu16
- create new containers: lxc launch base_ubuntu16 <name>

Ansible

- Inventory File: /root/ansible101/demo/hosts
- Cong File: /root/ansible101/demo/ansible.cfg




